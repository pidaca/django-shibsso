#
# Copyright 2010 CERN
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Login and logout views.
Because these are Shibboleth (R) webpages, these views can not be customized.

"""

import re

from django.conf import settings
from django.contrib import auth
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.http import urlquote
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache

def login(request, redirect_field_name=REDIRECT_FIELD_NAME):
    """Displays the login form and handles the login action."""

    redirect_to = request.GET.get(redirect_field_name, '')

    if request.META.get(settings.META_USERNAME):

        user = auth.authenticate(request=request)

        if not user:
            raise PermissionDenied

        if not redirect_to or ' ' in redirect_to:
            redirect_to = settings.LOGIN_REDIRECT_URL
        elif '//' in redirect_to and re.match(r'[^\?]*//', redirect_to):
            redirect_to = settings.LOGIN_REDIRECT_URL

        auth.login(request, user)

        return HttpResponseRedirect(redirect_to)

    path = urlquote(request.build_absolute_uri(request.get_full_path()))

    return HttpResponseRedirect('https://%s%s%s' %
                                (request.get_host(),
                                settings.SHIB_LOGIN_PATH,
                                path))

login = never_cache(login)


def logout(request):
    auth.logout(request)

    return HttpResponseRedirect(settings.SHIB_LOGOUT_URL)


logout = never_cache(logout)
